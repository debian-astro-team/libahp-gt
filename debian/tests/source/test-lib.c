#include <stdio.h>
#include <ahp/ahp_gt.h>
#include <stdint.h>


int main(int argc, char** argv)
{
 int rc=0;
 int version=0;
 int isConnected;

 version=ahp_gt_get_version();

 if (version==0) {
   fprintf(stderr,"D: initial version may not be zero");
   rc=1;
 }
 isConnected=ahp_gt_is_connected();
 printf("D: isConnected: %i\n",isConnected);
 /* we can not be connected, so this is an error */
 if (isConnected!=0) rc=1;

 if (rc==0) {
   printf("D: PASS\n");
 }

 return rc;
}
